const Joi = require('joi');

// define a register validation module
const registerValidation = (data) => {

    const schema = {
        name: Joi.string().min(6).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    };
    return Joi.validate(data,schema);

};

// define a login validation module
const loginValidation = data => {

    const schema = {
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    };
    return Joi.validate(data,schema);

};
 
module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;